Images for [blog.jwf.io](https://blog.jwf.io)
=============================================

Featured images for posts on Justin W. Flory's blog.


## What

This repository contains the source SVGs used for blog posts that I author for my [personal blog](https://blog.jwf.io).
Inspired by [fedoramagazine-images](https://pagure.io/fedoramagazine-images) for the [Fedora Magazine](https://fedoramagazine.org), I create unique featured images for my posts.


## Legal

[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

[Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) unless otherwise stated.
